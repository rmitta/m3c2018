! Lab 4, task 1

program task1
    implicit none
    integer :: i1
    integer, parameter :: N=5, display_iteration = 1
    real(kind=8) :: odd_sum

    call computeSum(N, display_iteration, odd_sum)

    print *, 'odd_sum=', odd_sum

end program task1




subroutine computeSum(N, display_iteration, odd_sum)
    implicit none
    integer, intent(in) :: N, display_iteration
    real(kind=8), intent(out) :: odd_sum
    integer :: i1

    odd_sum = 0.d0
    do i1 = 1,N
        if (display_iteration == 1) then
            print *, 'odd_sum=',odd_sum
        end if
        odd_sum = odd_sum + 2*i1 - 1
    end do
end subroutine computeSum

 ! To compile this code:
  ! $ gfortran -o task1.exe lab41.f90
  ! To run the resulting executable: $ ./task1.exe
